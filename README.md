![version 0.0.1](https://img.shields.io/badge/version-0.0.1-yellow.svg?style=flat-square)

# README #

> Markdown rendering in Bitbucket is quite shitty. Better vie the README.md in e.g. VSCODE with the Extension ``Markdown Preview Github Styling``

This README covers basic information about this extension and special features of the implementation.

## Structure ##

![](Readme/datastructure.png)

## How do I create a form? ##

Create one or some folders in the TYPO3 page-tree for ``Fields``, ``Fieldsets``, ``Steps``, ``Forms``, ``Constraints`` and  (confirmation) ``Mails`` to sender (user) and receiver (admin).

Begin with ``Fields``, ``Fieldsets``, ``Steps`` and ``Mails`` **in this particularly order**. Then create the ``Form(s)`` and ``Constraints``. ``Fields``, ``Fieldsets``, ``Steps`` and ``Mails`` can be used in _multiple_ ``Forms``. ``Constraints`` are unique for one single ``Form`` and ``Field`` combination.

### How do i configure mail shipment? ### 

Assign ``Mail`` objects to the ``form``.

* ``MailToSenderConfirm`` => Double-Opt-In Mail
* ``MailToSender`` => Mail to the user at the end of the process (with or without Double-Opt-In)
* ``MailToReceiver`` => Mail to the admin

#### Define SenderMail and -Name ####

Add an ``input`` and ``email`` field tou your form and add a mandatory ``Constraint`` for the fields. While beeing logged in as a Backenduser in TYPO3 you can see ``badges`` with a number above mandatory ``input`` and ``email`` fields (see image below). Copy the numbers and put them to your form in the backend. Use the fields ``Field Sender Mail`` and/or ``Field Sender (First) Name``. 

![](Readme/senderNameAndMail.png)

_An example of two mandatory fields with numbered ``badges``_

## What's in the code? ##

### RenderController ###

The ``RenderController`` is the main Controller of ``kitt3n_form`` Extension for TYPO3 CMS. The RenderController injects needed ``Repositories`` and ``Services``, renders validates and sends mails for session based multistep forms using Extbase models and Symfony components. Generally we make heavy use of Symfony components to create ``FormViews``, ``FieldTypes`` and ``Validation``.

#### Slots ####

The ``addFieldToForm()`` method contains slots to override ``Readonly`` and ``Hidden`` field values (use e.g. in a career extension). 

See example in ``/ext_localconf.php`` and corresponding ``Classes/Slot/RenderControllerSlot.php``

### StorageService ###

Handles file uploads and generates ``JSON``, ``YAML`` and ``XML`` files containing submitted field values.

### SessionService ###

Submitted values are stored in a ``PHPSESSION``. Symfony either creates an own session or uses an existing one if already open. All submitted fields are saved into the active session. Fieldnames / Session keys are standardized. For more detailed information take a look at the ``PatternService``.

### TranslationService ###

``Kitt3nForm`` uses copied and renamed ``*.xlf`` files for validation/error messages. Symfony uses a different naming scheme ``validators.<language>.xlf`` than Extbase.

In the files Symfony uses Numbers as ``IDs``. In the validation process it uses -- I think -- some kind of ``MD5 Hashes`` for it's error messages. The error messages itself can also contain placeholder e.g ``{{ placeholder }}`` that have to be replaced in the Controller and/or Service.  

```
<trans-unit id="1">
    <source>This value should be false.</source>
    <target>This value should be false.</target>
</trans-unit>
``` 

_Example error message with Number as ``ID``_

We therefore assign these error message ``Hashes`` to the corresponding ``IDs`` in our ``TranslationService``.

```
vendor
└── symfony
    └── validator
        └── Resources
            └── translations
                ├── af.validators.xlf
                ├── ar.validators.xlf
                .
                .
                .
                ├── de.validators.xlf
                .
                .
                .
                ├── validators.xlf
                ├── vi.validators.xlf
                ├── zh_CN.validators.xlf
                └── zh_TW.validators.xlf
```

_File structure of customized Symfony ``*.xlf`` files in ``Resources/Private/Language``_

## DSGVO Todos ##

> **Add/Create** ``.htaccess`` (Deny from all) in ``StorageService`` when initializing mail folders and/or when saving files.

> **Encrypt** uploaded files (GnuPG, Private/Public Key encryption)

> **Encrypt** ``JSON``, ``YAML`` and ``XML`` files (GnuPG, Private/Public Key encryption)

## Todos ##

> **Implement** handling arguments/parameters from ``third party extensions`` (e.g. career). 

> **Implement** PDF generation in ``StorageService``.

> **Remove** unused variables in ``constants.ts`` and ``setup.ts``.

> **Split** Template into Partials for better maintainability.

> **Fix** ``MM`` configuration in all ``TCA`` overrides. 

```
## Add 

'foreign_table_where' => 
'AND  tx_kitt3nform_domain_model_<whatever>.sys_language_uid IN (-1,0) 
AND tx_kitt3nform_domain_model_<whatever>.deleted = 0 
AND tx_kitt3nform_domain_model_<whatever>.hidden = 0'
```

> **Implement** more slots.

> **Implement** more ``FieldTypes`` e.g. ``Captcha``, ``RadioButtons``.

## IMPORTANT ##

**Never use a form on a site without a SSL certificate because of the ``DSGVO``.** 
