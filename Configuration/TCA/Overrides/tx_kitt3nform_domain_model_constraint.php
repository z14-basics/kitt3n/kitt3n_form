<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sTable = basename(__FILE__, '.php');
$sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\Constraint';
$sUserFuncPlugin = 'tx_kitt3nform';

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_form/Resources/Public/Icons/' . $sModel . '.svg';


$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'form';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'field';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

/*
 * Form
 */
$sColumn = 'form';
$sTable = 'tx_kitt3nform_domain_model_form';
$sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\Form';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'minitems' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'fieldControl' => [
        'addRecord' => [
            'disabled' => 0,
            'options' => [
                'setValue' => 'append'
            ],
        ],
    ],
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) KITT3N\Kitt3nUserfuncs\UserFunc\StorageUserFunc::getStoragePidListForModelInPlugin($sUserFuncModel, $sPlugin);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}
$sFirstPid = (string) KITT3N\Kitt3nUserfuncs\UserFunc\StorageUserFunc::getFirstStoragePidForModelInPlugin($sUserFuncModel, $sPlugin);
if ($sFirstPid != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['fieldControl']['addRecord']['options']['pid'] = $sFirstPid;
}

/*
 * Field
 */
$sColumn = 'field';
$sTable = 'tx_kitt3nform_domain_model_field';
$sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\Field';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'minitems' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'fieldControl' => [
        'addRecord' => [
            'disabled' => 0,
            'options' => [
                'setValue' => 'append'
            ],
        ],
    ],
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) KITT3N\Kitt3nUserfuncs\UserFunc\StorageUserFunc::getStoragePidListForModelInPlugin($sUserFuncModel, $sPlugin);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}
$sFirstPid = (string) KITT3N\Kitt3nUserfuncs\UserFunc\StorageUserFunc::getFirstStoragePidForModelInPlugin($sUserFuncModel, $sPlugin);
if ($sFirstPid != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['fieldControl']['addRecord']['options']['pid'] = $sFirstPid;
}

/*
 * Field
 */
$sColumn = 'mime_type';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectCheckBox',
    'items' => [
        ['*.pdf', 'application/pdf'],
        ['*.doc', 'application/msword'],
        ['*.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
        ['*.odt',  'application/vnd.oasis.opendocument.text'],
        ['*.xls', 'application/vnd.ms-excel'],
        ['*.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
        ['*.ods', 'application/vnd.oasis.opendocument.spreadsheet'],
        ['*.ppt', 'application/vnd.ms-powerpoint'],
        ['*.pptx', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'],
        ['*.zip', 'application/zip'],
        ['*.jpeg *.jpg', 'image/jpeg'],
        ['*.png', 'image/png'],
        ['*.gif', 'image/gif'],
        ['*.bmp', 'image/bmp'],
    ],
];

/*
 * maxFileSizeUnit
 */
$sColumn = 'max_file_size_unit';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
        ['M', 'M'],
        ['K', 'K'],
    ],
];

