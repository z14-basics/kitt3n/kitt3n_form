<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\MailToSenderConfirm';
$sUserFuncPlugin = 'tx_kitt3nform';

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_form/Resources/Public/Icons/' . $sModel . '.svg';

/*
 * From name
 */
$sColumn = 'from_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "Musterfirma GmbH";

/*
 * From mail
 */
$sColumn = 'from_mail';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "info@musterfirma.de";

/*
 * From subject
 */
$sColumn = 'from_subject';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "Please confirm your mail address";

/*
 * From salutation
 */
$sColumn = 'from_salutation';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "Dear Customer,";

/*
 * From body before link
 */
$sColumn = 'from_body_before_link';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>thank you for your request.<br><br>Please confirm your request by clicking on the link below, in order to prevent any misuse of your e-mail address by third parties. Only then your request is complete and can be processed by us.</p>";

/*
 * From body after link
 */
$sColumn = 'from_body_after_link';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>If the verification link may not work in your email program, copy it into the address bar of your web browser; only then your request is confirmed.</p>";

/*
 * From complimentary close
 */
$sColumn = 'from_complimentary_close';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>Best regards,<br>Max Mustermann</p>";

/*
 * From footer
 */
$sColumn = 'from_footer';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>Musterfirma GmbH<br><br>Phone: +49 XXXX XXX XXX<br>Fax: +49 XXXX XXX XXXX<br>E-mail: info@musterfirma.de<br><br>Musterfirma GmbH<br>Musterstr. X | D-XXXXX Musterstadt<br>Registergericht: Mustergericht, HRB XXXXX<br>Geschäftsführer: Max Mustermann<br>www.musterfirma.de</p>";

/*
 * ReplyTo name
 */
$sColumn = 'reply_to_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "Musterfirma GmbH";

/*
 * ReplyTo mail
 */
$sColumn = 'reply_to_mail';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "reply.to@musterfirma.de";