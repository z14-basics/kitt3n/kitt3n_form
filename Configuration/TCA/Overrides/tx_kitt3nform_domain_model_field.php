<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sTable = basename(__FILE__, '.php');
$sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\Field';
$sUserFuncPlugin = 'tx_kitt3nform';

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_form/Resources/Public/Icons/' . $sModel . '.svg';

/*
 * Type
 */
$sColumn = 'type';
// $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
// $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingleBox',
    'items' => [
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.Input',
            '--div--'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.TextType',
            'TextType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.TextareaType',
            'TextareaType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.EmailType',
            'EmailType'
        ],
//        [
//            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.PasswordType',
//            'PasswordType'
//        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.HiddenType',
            'HiddenType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.ReadonlyType',
            'ReadonlyType'
        ],

        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.Choice',
            '--div--'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.CheckboxType',
            'CheckboxType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.CheckboxesType',
            'CheckboxesType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.ChoiceType',
            'ChoiceType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.RadioType',
            'RadioType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.CountryType',
            'CountryType'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.LanguageType',
            'LanguageType'
        ],

//        [
//            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.Optgroup.Date',
//            '--div--'
//        ],
//        [
//            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.DateTimeType',
//            'DateTimeType'
//        ],

        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.File',
            '--div--'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.FileType',
            'FileType'
        ],

//        [
//            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.Optgroup.Button',
//            '--div--'
//        ],
//        [
//            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.ResetType',
//            'ResetType'
//        ],
//        [
//            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.SubmitType',
//            'SubmitType'
//        ],

        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.Others',
            '--div--'
        ],
        [
            'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Html',
            'Html'
        ],
    ],
    'size' => 21,
    'maxitems' => 1,
    'eval' => 'required',
    'onChange' => 'reload'
];