<?php

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_form/Resources/Public/Icons/' . $sModel . '.svg';