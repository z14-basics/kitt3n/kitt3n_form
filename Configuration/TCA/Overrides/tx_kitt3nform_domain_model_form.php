<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\Form';
$sUserFuncPlugin = 'tx_kitt3nform';

$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'uid';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:kitt3n_form/Resources/Public/Icons/' . $sModel . '.svg';

$GLOBALS['TCA'][$sModel]['types'] = [
    '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, text_submit, text_confirm, storage_path, storage_confirm_path, step, field_sender_first_name, field_sender_name, field_sender_mail, mail_to_sender_confirm, mail_to_sender, mail_to_receiver, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
];

/*
 * Description
 * => Use this field for overriding the submit button text
 */
$sColumn = 'description';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['rows'] = 1;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['label'] = 'LLL:EXT:kitt3n_form/Resources/Private/Language/translation_db.xlf:tx_kitt3nform_domain_model_form.description';

/*
 * Type
 */
$sColumn = 'step';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['minitems'] = 1;

/*
 * Step
 */
$sColumn = 'step';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['enableMultiSelectFilterTextfield'] = true;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['foreign_table_where'] = ' AND tx_kitt3nform_domain_model_step.hidden = 0 AND tx_kitt3nform_domain_model_step.deleted = 0 AND tx_kitt3nform_domain_model_step.sys_language_uid IN (-1,0)';

/*
 * Mail to sender confirm
 */
$sColumn = 'mail_to_sender_confirm';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['renderType'] = 'selectMultipleSideBySide';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['eval'] = 'int';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = 0;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['enableMultiSelectFilterTextfield'] = true;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['foreign_table_where'] = ' AND tx_kitt3nform_domain_model_mailtosenderconfirm.hidden = 0 AND tx_kitt3nform_domain_model_mailtosenderconfirm.deleted = 0 AND tx_kitt3nform_domain_model_mailtosenderconfirm.sys_language_uid IN (-1,0)';

/*
 * Mail to sender
 */
$sColumn = 'mail_to_sender';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['renderType'] = 'selectMultipleSideBySide';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['eval'] = 'int';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = 0;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['enableMultiSelectFilterTextfield'] = true;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['foreign_table_where'] = ' AND tx_kitt3nform_domain_model_mailtosender.hidden = 0 AND tx_kitt3nform_domain_model_mailtosender.deleted = 0 AND tx_kitt3nform_domain_model_mailtosender.sys_language_uid IN (-1,0)';

/*
 * Mail to receiver
 */
$sColumn = 'mail_to_receiver';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['renderType'] = 'selectMultipleSideBySide';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['eval'] = 'int';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = 0;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['enableMultiSelectFilterTextfield'] = true;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['foreign_table_where'] = ' AND tx_kitt3nform_domain_model_mailtoreceiver.hidden = 0 AND tx_kitt3nform_domain_model_mailtoreceiver.deleted = 0 AND tx_kitt3nform_domain_model_mailtoreceiver.sys_language_uid IN (-1,0)';

/*
 * Storage path
 */
$sColumn = 'storage_path';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['rows'] = 1;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "/fileadmin/kitt3n_form/default/";

/*
 * Storage confirm path
 */
$sColumn = 'storage_confirm_path';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['rows'] = 1;
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "/fileadmin/user_upload/kitt3n_form/default/";

/*
 * Text Submit
 */
$sColumn = 'text_submit';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>Dear Customer,</p><p>thank you for your request.</p><p>You will receive an email from within short. Please then confirm your request by clicking on the attached link, in order to prevent any misuse of your e-mail address by third parties. Only then your request is complete and can be processed by us.</p>";
/*
 * Text Submit
 */
$sColumn = 'text_confirm';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>Thank you for confirming your e-mail address.</p>";

/*
 * Field Sender First Name
 */
$sColumn = 'field_sender_first_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "<page_uid>_<form_uid>_<step_uid>_<fieldset_uid>_<field_uid>";

/*
 * Field Sender Name
 */
$sColumn = 'field_sender_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "<page_uid>_<form_uid>_<step_uid>_<fieldset_uid>_<field_uid>";

/*
 * Field Sender Mail
 */
$sColumn = 'field_sender_mail';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "<page_uid>_<form_uid>_<step_uid>_<fieldset_uid>_<field_uid>";