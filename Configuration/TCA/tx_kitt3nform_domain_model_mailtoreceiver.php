<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,from_name,from_mail,from_subject,to_name,to_mail,to_cc,to_bcc',
        'iconfile' => 'EXT:kitt3n_form/Resources/Public/Icons/tx_kitt3nform_domain_model_mailtoreceiver.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, from_name, from_mail, from_subject, to_name, to_mail, to_cc, to_bcc, append_data',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, from_name, from_mail, from_subject, to_name, to_mail, to_cc, to_bcc, append_data, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kitt3nform_domain_model_mailtoreceiver',
                'foreign_table_where' => 'AND {#tx_kitt3nform_domain_model_mailtoreceiver}.{#pid}=###CURRENT_PID### AND {#tx_kitt3nform_domain_model_mailtoreceiver}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.from_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_mail' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.from_mail',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'from_subject' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.from_subject',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'to_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.to_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'to_mail' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.to_mail',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'to_cc' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.to_cc',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'to_bcc' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.to_bcc',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'append_data' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_form/Resources/Private/Language/locallang_db.xlf:tx_kitt3nform_domain_model_mailtoreceiver.append_data',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
    
    ],
];
