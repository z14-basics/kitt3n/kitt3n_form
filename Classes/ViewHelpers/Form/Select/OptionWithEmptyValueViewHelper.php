<?php

namespace KITT3N\Kitt3nForm\ViewHelpers\Form\Select;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Fluid\ViewHelpers\Form\Select\OptionViewHelper;

/**
 * Adds custom `<option>` tags inside an `<f:form.select>`
 *
 * @api
 */
class OptionWithEmptyValueViewHelper extends OptionViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'option';

    /**
     * Initialize additional arguments available for this tag view helper.
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
    }

    /**
     * @return string
     */
    public function render()
    {
        parent::render();

        /*
         * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         * Add empty value attribute [START]
         */
        if (isset($this->arguments['value']) && $this->arguments['value'] == "") {
            $this->tag->addAttribute('value', '');
        }
        /*
         * Add empty value attribute [END]
         * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
         */

        return $this->tag->render();
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    protected function isValueSelected($value)
    {
        parent::isValueSelected($value);
    }
}
