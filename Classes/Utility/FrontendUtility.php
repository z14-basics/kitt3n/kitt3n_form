<?php
namespace KITT3N\Kitt3nForm\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * Class FrontendUtility
 *
 * @package KITT3N\Kitt3nForm\Utility
 */
class FrontendUtility extends AbstractUtility
{
    /**
     * Get charset for frontend rendering
     *
     * @return string
     */
    public static function getCharset()
    {
        return self::getTyposcriptFrontendController()->metaCharset;
    }
}