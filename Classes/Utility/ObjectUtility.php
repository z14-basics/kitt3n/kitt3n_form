<?php
namespace KITT3N\Kitt3nForm\Utility;

use TYPO3\CMS\Core\Database\DatabaseConnection;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Lang\LanguageService;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class ObjectUtility
 * @package KITT3N\Kitt3nForm\Utility
 */
class ObjectUtility extends AbstractUtility
{

    /**
     * @return TypoScriptFrontendController
     */
    public static function getTyposcriptFrontendController()
    {
        return parent::getTyposcriptFrontendController();
    }

    // /**
    //  * @return DatabaseConnection
    //  */
    // public static function getDatabaseConnection()
    // {
    //     return parent::getDatabaseConnection();
    // }

    /**
     * @return ObjectManager
     */
    public static function getObjectManager()
    {
        return parent::getObjectManager();
    }

    /**
     * @return ContentObjectRenderer
     */
    public static function getContentObject()
    {
        return parent::getContentObject();
    }

    /**
     * @return array
     */
    public static function getFilesArray()
    {
        return parent::getFilesArray();
    }

    /**
     * @return LanguageService
     */
    public static function getLanguageService()
    {
        return parent::getLanguageService();
    }
}