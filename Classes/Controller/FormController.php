<?php
namespace KITT3N\Kitt3nForm\Controller;

use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\IsNull;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\IsFalse;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Ip;
use Symfony\Component\Validator\Constraints\Uuid;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\IdenticalTo;
use Symfony\Component\Validator\Constraints\NotIdenticalTo;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Time;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * FormController
 */
class FormController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * formRepository
     *
     * @var \KITT3N\Kitt3nForm\Domain\Repository\FormRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $formRepository = null;

    /**
     * @param $aArray
     * @param string $sPrefix
     * @param string $sDivider
     * @return array
     */
    protected function flattenArray($aArray, $sPrefix = '', $sDivider = '/')
    {
        $result = [];
        foreach ($aArray as $key => $value) {
            $new_key = $sPrefix . (empty($sPrefix) ? '' : $sDivider) . $key;
            if (is_array($value)) {
                $result = array_merge($result, $this->flattenArray($value, $new_key));
            } else {
                $result[$new_key] = $value;
            }
        }
        return $result;
    }

    /**
     * @param array $aArray
     * @param string $sCleanFrom
     * @return array
     */
    private function cleanUpArray($aArray = [], $sClean = '__')
    {
        $aCleanArray = $aArray;
        foreach ($aCleanArray as $key => $val) {
            if (strpos($key, $sClean) !== false) {
                unset($aCleanArray[$key]);
            } else {
                if (is_array($val)) {
                    $aCleanArray[$key] = $this->cleanUpArray($val);
                } else {
                    $aCleanArray[$key] = $val;
                }
            }
        }
        return $aCleanArray;
    }

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {
        //TODO https://symfony.com/doc/current/components/http_foundation/sessions.html
        //TODO https://symfony.com/doc/current/components/http_foundation/session_php_bridge.html
        //TODO Steps
        //TODO Fieldsets
        if (\PHP_SESSION_ACTIVE === session_status()) {
            /*
             * TYPO3 session started already
             */

            $session = new Session(new PhpBridgeSessionStorage());
        } else {
            /*
             * No TYPO3 session started
             */

            $session = new Session();
        }
        /*
         * symfony will now interface with the existing PHP session
         * or start a new one on its own
         */

        $session->start();
        $sSessionId = $session->getId();
        $aSessionAll = $session->all();
        /*
         * Request object
         */

        $request = Request::createFromGlobals();
        $aRequestParameters = $request->request->all();
        $aSession = [];
        if (count($aRequestParameters) > 0) {
            $aCleanParameters = $this->cleanUpArray($aRequestParameters);
            $session->set(key($aCleanParameters), $aCleanParameters[key($aCleanParameters)]);
        }
        //        $s = $request->getSession();
        $defaults = [

        ];
        $iTotalSteps = 3;
        $iRenderStep = 1;
        $aFormFactory = [];
        $aForm = [];
        $aFormView = [];
        for ($i = 1; $i <= $iTotalSteps; $i++) {
            $aFormFactory[$i] = Forms::createFormFactoryBuilder()->addExtension(new ValidatorExtension(Validation::createValidator()))->getFormFactory();
            $name = 'tx_kitt3nform_kitt3nformformrender_step_' . $i;
            $prefix = 'field_';
            $suffix = '_' . $i;
            $aForm[$i] = $aFormFactory[$i]->createNamedBuilder($name, FormType::class, $defaults)->getForm();
            $sKey = $prefix . 'task' . $suffix;
            $aForm[$i]->add($sKey, TextType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\Length(['min' => 3])
                ],
                'label' => 'Task',
                'data' => $i
            ]);
            $aForm[$i]->add('step', HiddenType::class, [
                'data' => $i
            ]);
            $aForm[$i]->add('prev', SubmitType::class, [
                'label' => '<<'
            ]);
            $aForm[$i]->add('next', SubmitType::class, [
                'label' => $i == $iTotalSteps ? 'send' : '>>'
            ]);
            $aForm[$i]->handleRequest();
            $aFormView[$i] = $aForm[$i]->createView();
            if ($aForm[$i]->isSubmitted()) {
                // TODO validate step
                $iSubmittedStep = intval($aFormView[$i]->children['step']->vars['value']);
                // TODO prev and next
                if ($iSubmittedStep < $iTotalSteps) {
                    $iRenderStep = $iSubmittedStep + 1;
                } else {
                    // TODO send form
                    $iRenderStep = $iSubmittedStep;
                }
                print $iSubmittedStep;
            }
        }
        //        $formFactory = Forms::createFormFactoryBuilder()
        //                            ->addExtension(new ValidatorExtension(Validation::createValidator()))
        //                            ->getFormFactory();//Forms::createFormFactory();
        ////        $form = $formFactory->createBuilder(FormType::class, $defaults)->getForm();
        //        $form = $formFactory->createNamedBuilder('tx_kitt3nform_kitt3nformformrender', FormType::class, $defaults)->getForm();
        //         * Add fields
        //        $form->add('task', TextType::class, array(
        //            'constraints' => array(
        //                new Assert\NotNull(),
        //                new Assert\Length(array('min' => 3)),
        //            ),
        //            'label' => 'Task'
        //        ));
        //        $form->add('task1', TextType::class, array(
        //            'constraints' => array(
        //                new Assert\NotNull(),
        //                new Assert\Length(array('min' => 3)),
        //            ),
        //            'label' => 'Task 1'
        //        ));
        //        $form->add('country', CountryType::class);
        ////        $form->add('dueDate', DateType::class);
        //        $form->add('submit', SubmitType::class);
        //         * Add Field constraints
        //        $aFields = [];
        //        $aFields['task'] = [
        //                new Assert\Length(['min' => 10]),
        //                new Assert\NotNull()
        //            ];
        //        $constraint = new Assert\Collection(
        //            [
        //                // the keys correspond to the keys in the input array
        //                'fields' => $aFields,
        //                'allowExtraFields' => true, //http://symfony.com/doc/current/reference/constraints/Collection.html#allowextrafields
        //                'allowMissingFields' => false, //http://symfony.com/doc/current/reference/constraints/Collection.html#allowmissingfields
        //            ]
        //        );
        ////        $request = Request::createFromGlobals();
        //        $form->handleRequest();
        //        $formView = $form->createView();
        //        if ($form->isSubmitted()) {
        //             * Error handling
        //            foreach ($formView->children as $formViewChild) {
        //                /* @var \Symfony\Component\Form\FormErrorIterator $e */
        ////                $e = $formViewChild->vars['errors'];
        ////                $ee = $e->count();
        ////                 $x = 1;
        //            }
        ////            $formViewData = $form->getViewData();
        ////             * https://symfony.com/doc/current/validation/raw_values.html
        ////            $validator = Validation::createValidator();
        ////            $oViolations = $validator->validate($formViewData, $constraint);
        //        }
        /* Maybe use Twig or Symfony PhpEngine instead of Fluid ??
         * https://symfony.com/doc/current/components/form.html#twig-templating
         * https://symfony.com/doc/3.3/components/templating.html
         */

        var_dump($_SESSION);
        $this->view->assign('form', $aFormView[$iRenderStep]);
    }
}
