<?php

namespace KITT3N\Kitt3nForm\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class SessionService
 * @package KITT3N\Kitt3nForm\Service
 */
class SessionService implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * patternService
     *
     * @var \KITT3N\Kitt3nForm\Service\PatternService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $patternService = null;

    public $activeSession = null;

    public function setActiveSession ($session) {
        $this->activeSession = $session;
    }

    public function getActiveSession () {
        return $this->activeSession;
    }

    public function initializeSession()
    {
        if (\PHP_SESSION_ACTIVE === session_status()) {
            /**
             * TYPO3 session started already
             */
            $session = new Session(new PhpBridgeSessionStorage());
        } else {
            /**
             * No TYPO3 session started
             */
            $session = new Session();
        }
        /**
         * symfony will now interface with the existing PHP session
         * or start a new one on its own
         */
        $session->start();

        $this->setActiveSession($session);

        return $session;
    }

    /**
     * @param array $aRequestParameters
     * @param $session
     */
    public function saveRequestParametersIntoSession($aRequestParameters = [], $session = null)
    {
        $aCleanParameters = $this->cleanUpArray($aRequestParameters);
        foreach ($aCleanParameters as $sFieldsInStepKey => $aFieldsInStep) {
            foreach ($aFieldsInStep as $sFieldName => $sFieldValue) {
                /*
                 * Handle empty checkbox
                 *
                 * @see also ...ViewHelper
                 */
                if (strpos($sFieldName, '--empty_checkbox') !== false) {
                    $aEmptyCheckboxFieldName = explode("--", $sFieldName);
                    $sFieldnameEmptyCheckbox = $aEmptyCheckboxFieldName[0];
                    if ( ! array_key_exists($sFieldnameEmptyCheckbox, $aFieldsInStep)) {
                        /*
                         * Workaround for empty checkboxes
                         *
                         * Checkbox with name '$sFieldnameEmptyCheckbox' not checked
                         * => not in $_POST
                         *
                         * See also: Template and RenderController
                         */
                        $session->set($sFieldnameEmptyCheckbox, $sFieldValue);
                    }
                }
                $session->set($sFieldName, $sFieldValue);
            }
        }
    }

    /**
     * @param array $aRequestParameters
     * @param $session
     */
    public function saveRequestParametersIntoActiveSession($aRequestParameters = [])
    {
        $session = $this->getActiveSession();
        $aCleanParameters = $this->cleanUpArray($aRequestParameters);
        foreach ($aCleanParameters as $sFieldsInStepKey => $aFieldsInStep) {
            foreach ($aFieldsInStep as $sFieldName => $sFieldValue) {
                /*
                 * Handle empty checkbox
                 *
                 * @see also ...ViewHelper
                 */
                if (strpos($sFieldName, '--empty_checkbox') !== false) {
                    $aEmptyCheckboxFieldName = explode("--", $sFieldName);
                    $sFieldnameEmptyCheckbox = $aEmptyCheckboxFieldName[0];
                    if ( ! array_key_exists($sFieldnameEmptyCheckbox, $aFieldsInStep)) {
                        /*
                         * Workaround for empty checkboxes
                         *
                         * Checkbox with name '$sFieldnameEmptyCheckbox' not checked
                         * => not in $_POST
                         *
                         * See also: Template and RenderController
                         */
                        $session->set($sFieldnameEmptyCheckbox, $sFieldValue);
                    }
                }
                $session->set($sFieldName, $sFieldValue);
            }
        }
    }

    /**
     * @param string $sFieldName
     * @param string $sFieldValue
     * @param $session
     *
     * @return boolean
     */
    public function saveParameterIntoSession($sFieldName = '', $sFieldValue = '', $session = null)
    {
        if ($session != null and $sFieldName != '') {
            $session->set($sFieldName, $sFieldValue);
            return true;
        }
        return false;
    }

    /**
     * @param string $sFieldName
     * @param string $sFieldValue
     * @param $session
     */
    public function saveParameterIntoActiveSession($sFieldName = '', $sFieldValue = '')
    {
        $session = $this->getActiveSession();
        $session->set($sFieldName, $sFieldValue);
    }

    /**
     * @param int $iPageUid
     * @param int $iElementUid
     * @param int $iFormUid
     *
     * @return bool
     */
    public function resetActiveSessionForGivenForm ($iFormUid = 0)
    {
        if ($iFormUid != 0) {
            $aPatternConst = $this->patternService->getConstArray();
            $sProcessNumberPattern = $this->patternService->getProcessNumberPattern();
            $sProcessNumberSessionFieldName = str_replace([
                $aPatternConst['PROCESS_NUMBER_PLACEHOLDER__FORM_UID']
            ],
                [$iFormUid], $sProcessNumberPattern
            );
            $iCountProcessNumberSessionFieldName = strlen($sProcessNumberSessionFieldName);
            $aActiveSession = $this->getActiveSession();
            $aSessionAttributes = $aActiveSession->getBag('attributes')->all();

            foreach ($aSessionAttributes as $sKey => $sValue) {
                if (substr($sKey, 0, $iCountProcessNumberSessionFieldName) == $sProcessNumberSessionFieldName) {
                    $this->saveParameterIntoActiveSession($sKey, '');
                }
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * @param string $sFieldName
     * @param $session
     */
    public function getParameterFromSession($sFieldName = '', $session = null)
    {
        return $session != null ? $session->get($sFieldName) : null;
    }

    /**
     * @param string $sFieldName
     * @param $session
     */
    public function getParameterFromActiveSession($sFieldName = '')
    {
        $session = $this->getActiveSession();
        return $session->get($sFieldName);
    }

    /**
     * @param array $aArray
     * @param string $sClean
     *
     * @return array
     */
    private function cleanUpArray($aArray = [], $sClean = '__')
    {
        $aCleanArray = $aArray;
        foreach ($aCleanArray as $key => $val) {
            if (strpos($key, $sClean) !== false) {
                unset($aCleanArray[$key]);
            } else {
                if (is_array($val)) {
                    $aCleanArray[$key] = $this->cleanUpArray($val);
                } else {
                    $aCleanArray[$key] = $val;
                }
            }
        }

        return $aCleanArray;
    }

    public function getProcessNumberFromActiveSession ($iFormUid = 0)
    {
        $aPatternConst = $this->patternService->getConstArray();
        $activeSession = $this->getActiveSession();
        if($activeSession != null and $iFormUid > 0) {
            $sProcessNumberPattern = $this->patternService->getProcessNumberPattern();
            $sProcessNumberSessionFieldName = str_replace([
                $aPatternConst["PROCESS_NUMBER_PLACEHOLDER__FORM_UID"],
            ], [$iFormUid], $sProcessNumberPattern);
            return $this->getParameterFromSession($sProcessNumberSessionFieldName, $activeSession);
        } else {
            return null;
        }
    }

    public function getProcessNumberFromSession ($iFormUid = 0, $session = null)
    {
        $aPatternConst = $this->patternService->getConstArray();
        if($session != null and $iFormUid > 0) {
            $sProcessNumberPattern = $this->patternService->getProcessNumberPattern();
            $sProcessNumberSessionFieldName = str_replace([
                $aPatternConst["PROCESS_NUMBER_PLACEHOLDER__FORM_UID"],
            ], [$iFormUid], $sProcessNumberPattern);
            return $this->getParameterFromSession($sProcessNumberSessionFieldName, $session);
        } else {
            return null;
        }
    }
}