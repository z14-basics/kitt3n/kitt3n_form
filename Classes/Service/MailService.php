<?php

namespace KITT3N\Kitt3nForm\Service;

use Swift_Attachment;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use KITT3N\Kitt3nForm\Utility\ObjectUtility;
use KITT3N\Kitt3nForm\Utility\FrontendUtility;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;
use ReflectionClass;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class MailService
 * @package KITT3N\Kitt3nForm\Service
 */
class MailService implements \TYPO3\CMS\Core\SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $configurationManager;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $persistenceManager;

    /**
     * @var \KITT3N\Kitt3nForm\Service\PlaintextService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $plaintextService;

    /**
     * sessionService
     *
     * @var \KITT3N\Kitt3nForm\Service\SessionService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $sessionService = null;

    /**
     * storageService
     *
     * @var \KITT3N\Kitt3nForm\Service\StorageService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $storageService = null;

    /**
     * patternService
     *
     * @var \KITT3N\Kitt3nForm\Service\PatternService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $patternService = null;

    /**
     * translationService
     *
     * @var \KITT3N\Kitt3nForm\Service\TranslationService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $translationService = null;

    /**
     * @var Content Object
     */
    protected $cObj;

    /**
     * @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher
     */
    protected $signalSlotDispatcher = null;

    /**
     * @param \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher
     */
    public function injectSignalSlotDispatcher(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher)
    {
        $this->signalSlotDispatcher = $signalSlotDispatcher;
    }


    /**
     * @param \KITT3N\Kitt3nForm\Domain\Model\Form $oForm
     * @param string $sUrl
     * @param \KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm $oMail
     *
     * @return bool
     */
    public function sendMailToSenderConfirm($oForm, $sUrl, $oMail)
    {
        /** @var MailMessage $message */
        $message = ObjectUtility::getObjectManager()->get(MailMessage::class);

        if ( ! empty($oForm->getFieldSenderMail()) and GeneralUtility::validEmail($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail()))) {
            $sToMail = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail());
            $sToName = $sToMail;
            if ( ! empty($oForm->getFieldSenderName()) and $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName()) != '') {
                if (empty($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderFirstName()))) {
                    $sToName = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName());
                } else {
                    $sToName = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderFirstName()) . ' ' .
                               $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName());
                }
            }

            $message
                ->setTo([$sToMail => $sToName])
                ->setFrom([$oMail->getFromMail() => $oMail->getFromName()])
                ->setSubject($oMail->getFromSubject())
                ->setCharset(FrontendUtility::getCharset())
                ->setSender($oMail->getFromMail(), $oMail->getFromName());

            if ( ! empty($oMail->getReplyToMail()) && ! empty($oMail->getReplyToName())) {
                $message->setReplyTo([$oMail->getReplyToMail() => $oMail->getReplyToName()]);
            }

            $oClass = new ReflectionClass('\\KITT3N\\Kitt3nForm\\Service\\TranslationService');
            $aConst = $oClass->getConstants();
            $sConfirm = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                $aConst['DEFAULT_TRANSLATION_PATH'] . 'tx_kitt3nform.mailToSenderConfirm.confirm', $aConst['EXTENSION_KEY']);


            $sBaseTag = $this->getBaseUrl();
            $sUrlWithBaseTag = $sBaseTag . $sUrl;

//            $message = $this->addHtmlBody($message, $aMail);
            $sBody = "<p>" . $oMail->getFromSalutation() . "</p>" . $oMail->getFromBodyBeforeLink();
            $sBody .= "<p><u><a href='$sUrlWithBaseTag'>$sConfirm</a></u></p>";
            $sBody .= $oMail->getFromBodyAfterLink() . $oMail->getFromComplimentaryClose() . $oMail->getFromFooter();

            $message = $this->addPlainBody($message, $sBody);
            $message->send();

            return $message->isSent();
        }
        return false;
    }

    /**
     * @param \KITT3N\Kitt3nForm\Domain\Model\Form $oForm
     * @param \KITT3N\Kitt3nForm\Domain\Model\MailToSender $oMail
     * @param string $sEmailFromToken
     * @param string $sProcessNumber
     * @param array $aSettings
     * @param array $aValue
     *
     * @return bool
     */
    public function sendMailToSender($oForm, $oMail, $sEmailFromToken = '', $sProcessNumber = '', $aSettings = [], $aValue = [])
    {
        /** @var MailMessage $message */
        $message = ObjectUtility::getObjectManager()->get(MailMessage::class);

        $aUnserializedDataFromFile = $this->getUnserializedDataFromFile($sProcessNumber, $aSettings);
        $aUnserializedSessionDataFromFile = $this->getUnserializedSessionDataFromFile($sProcessNumber, $aSettings);

        $sToMail = '';
        if( ! empty($sEmailFromToken)) {
            /*
             * Double opt in
             */
            if (GeneralUtility::validEmail($sEmailFromToken)) {
                $sToMail = $sEmailFromToken;
            }
        } else {
            if ( ! empty($oForm->getFieldSenderMail()) and GeneralUtility::validEmail($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail()))) {
                $sToMail = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail());
            }
        }

        if ( ! empty($sToMail)) {
            $sToName = $sToMail;

            if( ! empty($sEmailFromToken)) {
                /*
                 * Double opt in
                 */
                if ( ! empty($oForm->getFieldSenderName()) and array_key_exists($oForm->getFieldSenderName(),$aUnserializedSessionDataFromFile) and ! empty($aUnserializedSessionDataFromFile[$oForm->getFieldSenderName()])) {
                    $sToName = $aUnserializedSessionDataFromFile[$oForm->getFieldSenderName()];
                    if ( ! empty($oForm->getFieldSenderFirstName()) and array_key_exists($oForm->getFieldSenderFirstName(),$aUnserializedSessionDataFromFile) and ! empty($aUnserializedSessionDataFromFile[$oForm->getFieldSenderFirstName()])) {
                        $sToName = $aUnserializedSessionDataFromFile[$oForm->getFieldSenderFirstName()] . ' ' . $sToName;
                    }
                }
            } else {
                if ( ! empty($oForm->getFieldSenderName()) and $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName()) != '') {
                    if (empty($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderFirstName()))) {
                        $sToName = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName());
                    } else {
                        $sToName = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderFirstName()) . ' ' .
                            $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName());
                    }
                }
            }

            /*
             * Override $sSubject via Slot
             */
            $sSubject = $oMail->getFromSubject();
            $aUnserializedDataFromFile = $this->getUnserializedDataFromFile($sProcessNumber, $aSettings);
            $aUnserializedSessionDataFromFile = $this->getUnserializedSessionDataFromFile($sProcessNumber, $aSettings);
            $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideSubject', [
                &$sSubject,
                $aUnserializedDataFromFile,
                $aUnserializedSessionDataFromFile,
                $aValue
            ]);

            $message
                ->setTo([$sToMail => $sToName])
                ->setFrom([$oMail->getFromMail() => $oMail->getFromName()])
                ->setSubject($sSubject)
                ->setCharset(FrontendUtility::getCharset())
                ->setSender($oMail->getFromMail(), $oMail->getFromName());

            if ( ! empty($oMail->getReplyToMail()) && ! empty($oMail->getReplyToName())) {
                $message->setReplyTo([$oMail->getReplyToMail() => $oMail->getReplyToName()]);
            }

//            $message = $this->addHtmlBody($message, $aMail);
            /*
             * Append data to mail
             */
            $sData = "";
            if ($oMail->getAppendData()) {
                $sData = $this->getDataFromFileForMail($sProcessNumber, $aSettings);
            }

            /*
             * Override mail content via Slot
             */
            $sSalutation = $oMail->getFromSalutation();
            $sFromBody = $oMail->getFromBody();
            $sFromComplimentaryClose = $oMail->getFromComplimentaryClose();
            $sFromFooter = $oMail->getFromFooter();
            $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideMailContent', [
                &$sSalutation,
                &$sFromBody,
                &$sData,
                &$sFromComplimentaryClose,
                &$sFromFooter,
                $aUnserializedDataFromFile,
                $aUnserializedSessionDataFromFile,
                $aValue
            ]);

            $sBody = "<p>" . $sSalutation . "</p>" . $sFromBody . $sData . $sFromComplimentaryClose . $sFromFooter;

            $message = $this->addPlainBody($message, $sBody);
            $message->send();

            return $message->isSent();
        }
        return false;
    }


    /**
     * @param \KITT3N\Kitt3nForm\Domain\Model\Form $oForm
     * @param string $sProcessNumber
     * @param \KITT3N\Kitt3nForm\Domain\Model\MailToReceiver $oMail
     * @param array $aSettings
     * @param array $aValue
     *
     * @return bool
     */
    public function sendMailToReceiver($oForm, $sProcessNumber, $oMail, $aSettings= [], $aValue = [])
    {
        /** @var MailMessage $message */
        $message = ObjectUtility::getObjectManager()->get(MailMessage::class);

        if ( ! empty($oMail->getFromMail()) and GeneralUtility::validEmail($oMail->getFromMail())) {
            if ( ! empty($oMail->getFromName())) {

                $aUnserializedDataFromFile = $this->getUnserializedDataFromFile($sProcessNumber, $aSettings);
                $aUnserializedSessionDataFromFile = $this->getUnserializedSessionDataFromFile($sProcessNumber, $aSettings);

                $message
                    ->setFrom([$oMail->getFromMail() => $oMail->getFromName()])
                    ->setSender($oMail->getFromMail(), $oMail->getFromName());

                if ( ! empty($oMail->getToMail()) and GeneralUtility::validEmail($oMail->getToMail())) {
                    if ( ! empty($oMail->getToName())) {
                        $message->setTo([$oMail->getToMail() => $oMail->getToName()]);
                    }
                }

                if ( ! empty($oMail->getFromSubject())) {

                    /*
                     * Override $sSubject via Slot
                     */
                    $sSubject = $oMail->getFromSubject();
                    $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideSubject', [
                        &$sSubject,
                        $aUnserializedDataFromFile,
                        $aUnserializedSessionDataFromFile,
                        $aValue
                    ]);

                    $message->setSubject($sSubject);
                }

                if ( ! empty($oMail->getToCc())) {
                    $aToCcTmp = GeneralUtility::trimExplode(',', $oMail->getToCc(), true);
                    $aToCc = [];
                    foreach ($aToCcTmp as $sToCc) {
                        if (GeneralUtility::validEmail($sToCc)) {
                            $aToCc[] = $sToCc;
                        }
                    }
                    if (count($aToCc) > 0) {
                        $message->setCc($aToCc);
                    }
                }

                if ( ! empty($oMail->getToBcc())) {
                    $aToBccTmp = GeneralUtility::trimExplode(',', $oMail->getToBcc(), true);
                    $aToBcc = [];
                    foreach ($aToBccTmp as $sToBcc) {
                        if (GeneralUtility::validEmail($sToBcc)) {
                            $aToBcc[] = $sToBcc;
                        }
                    }
                    if (count($aToBcc) > 0) {
                        $message->setBcc($aToBcc);
                    }
                }

                if ( ! empty($oForm->getFieldSenderMail()) and GeneralUtility::validEmail($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail()))) {
                    $sReplyToMail = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail());
                    $sReplyToName = 'Reply to';
                    if ( ! empty($oForm->getFieldSenderName()) and $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName()) != '') {
                        if (empty($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderFirstName()))) {
                            $sReplyToName = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName());
                        } else {
                            $sReplyToName = $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderFirstName()) . ' ' .
                                            $this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderName());
                        }
                    }
                    $message->setReplyTo([$sReplyToMail => $sReplyToName]);
                }

                $message->setCharset(FrontendUtility::getCharset());

                $aStorage = $this->storageService->getStorageArrayFromSettings($aSettings);
                $sBody = "<p>RefNr: " . $sProcessNumber . "</p><p>Path: " . $aStorage['confirm']['absolute_path_from_webroot'] . $sProcessNumber . "/" . "</p>";

                /*
                 * Override mail body via Slot
                 */
                $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideMailBody', [
                    &$sBody,
                    $aUnserializedDataFromFile,
                    $aUnserializedSessionDataFromFile,
                    $aValue
                ]);

                /*
                 * Append data to mail
                 */
                $sData = "";
                if ($oMail->getAppendData()) {
                    $sData = $this->getDataFromFileForMail($sProcessNumber, $aSettings);

                    /*
                     * Override mail content via Slot
                     */
                    $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideMailContent', [
                        &$sData,
                        $aUnserializedDataFromFile,
                        $aUnserializedSessionDataFromFile,
                        $aValue
                    ]);

                    $sBody .= $sData;
                }

                $message = $this->addPlainBody($message, $sBody);

                $message->send();

                return $message->isSent();
            }
            return false;
        }
        return false;
    }


    /**
     * Returns the current base url
     *
     * @return string
     */
    protected function getBaseUrl(){
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['SERVER_NAME'];
    }


    /**
     * Add mail body plain
     *
     * @param MailMessage $message
     * @param string $sBody
     *
     * @return MailMessage
     */
    protected function addPlainBody(MailMessage $message, string $sBody)
    {

        $message->addPart($this->plaintextService->makePlain($sBody), 'text/plain');

        return $message;
    }

    /**
     * Set Sender Header according to RFC 2822 - 3.6.2 Originator fields
     *
     * @param MailMessage $message
     * @param array $aMail
     *
     * @return MailMessage
     */
    protected function addSenderHeader(MailMessage $message, array $aMail)
    {
        $email = $aMail['senderEmail'];
        $name = $aMail['senderName'];
        if (GeneralUtility::validEmail($email)) {
            if (empty($name)) {
                $name = null;
            }
            $message->setSender($email, $name);
        }

        return $message;
    }

    /**
     * @param string $sProcessNumber
     * @param array $aSettings
     */
    protected function getUnserializedSessionDataFromFile ($sProcessNumber = '', $aSettings = [])
    {
        $unserializedSession = [];
        $aStorage = $this->storageService->getStorageArrayFromSettings($aSettings);
        if ($aStorage != null) {
            $aConst = $this->patternService->getConstArray();
            $finder = new Finder();
            $finder->in($aStorage['confirm']['path_absolute'] . $sProcessNumber . '/');
            $finder->files()->name('session.serialized.txt');
            foreach ($finder as $file) {
                $contents = $file->getContents();
                if (is_string($contents) && strlen($contents) > 0) {
                    /*
                     * $unserializedPhpArrayContent = {array} [1]
                     *   steps = {array} [1]
                     *    1 = {array} [1]
                     *     fields = {array} [3]
                     *      0 = {array} [4]
                     *      1 = {array} [4]
                     *      2 = {array} [4]
                     *       name = "1_1_1_3"
                     *       type = "checkbox"
                     *       label = "..."
                     *       value = "1"
                     */
                    $unserializedSession = unserialize($contents);
                }
            }
        }
        return $unserializedSession;
    }

    /**
     * @param string $sProcessNumber
     * @param array $aSettings
     */
    protected function getUnserializedDataFromFile ($sProcessNumber = '', $aSettings = [])
    {
        $unserializedPhpArrayContent = [];
        $aStorage = $this->storageService->getStorageArrayFromSettings($aSettings);
        if ($aStorage != null) {
            $aConst = $this->patternService->getConstArray();
            $finder = new Finder();
            $finder->in($aStorage['confirm']['path_absolute'] . $sProcessNumber . '/');
            $finder->files()->name('data.serialized.txt');
            foreach ($finder as $file) {
                $contents = $file->getContents();
                if (is_string($contents) && strlen($contents) > 0) {
                    /*
                     * $unserializedPhpArrayContent = {array} [1]
                     *   steps = {array} [1]
                     *    1 = {array} [1]
                     *     fields = {array} [3]
                     *      0 = {array} [4]
                     *      1 = {array} [4]
                     *      2 = {array} [4]
                     *       name = "1_1_1_3"
                     *       type = "checkbox"
                     *       label = "..."
                     *       value = "1"
                     */
                    $unserializedPhpArrayContent = unserialize($contents);
                }
            }
        }
        return $unserializedPhpArrayContent;
    }

    /**
     * @param string $sProcessNumber
     * @param array $aSettings
     */
    protected function getDataFromFileForMail($sProcessNumber = '', $aSettings = [])
    {
        $unserializedPhpArrayContent = $this->getUnserializedDataFromFile($sProcessNumber, $aSettings);
        $sData = "";

        if (is_array($unserializedPhpArrayContent) && count($unserializedPhpArrayContent) > 0) {
            $aConst = $this->patternService->getConstArray();
            foreach ($unserializedPhpArrayContent['steps'] as $aStep) {
                $sData .= "<p>";
                foreach ($aStep['fields'] as $aField) {
                    $sData .= strip_tags(
                            str_replace(
                                [$aConst["FIELD__MANDATORY"], "&nbsp;"],
                                ["", " "],
                                $aField['label']),
                            "<a>") . "<br />";
                    $sData .= (is_array($aField['value']) ? implode(',', $aField['value']) : $aField['value']) . ((next($aStep['fields']) == true) ? "<br /><br />" : "");
                }
                $sData .= "</p>";
            }
        }

        if ($sData != "") {
            $sData = "<p>--</p>" . $sData .  "<p>--</p>";
        }
        return $sData;
    }
}