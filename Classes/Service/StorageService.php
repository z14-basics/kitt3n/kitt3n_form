<?php

namespace KITT3N\Kitt3nForm\Service;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

//use DeviceDetector\Yaml\Symfony;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\YamlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class UnitService
 * @package KITT3N\Kitt3nForm\Service
 */
class StorageService implements \TYPO3\CMS\Core\SingletonInterface
{

    protected $absolutePath = null;

    protected $confirmAbsolutePath = null;

    /**
     * @var null|string
     */
    protected $processNumber = null;

    /**
     * @return null
     */
    public function getAbsolutePath()
    {
        return $this->absolutePath;
    }

    /**
     * @param null $absolutePath
     */
    public function setAbsolutePath($absolutePath)
    {
        $this->absolutePath = $absolutePath;
    }

    /**
     * @return null
     */
    public function getConfirmAbsolutePath()
    {
        return $this->confirmAbsolutePath;
    }

    /**
     * @param null $confirmAbsolutePath
     */
    public function setConfirmAbsolutePath($confirmAbsolutePath)
    {
        $this->confirmAbsolutePath = $confirmAbsolutePath;
    }

    /**
     * @return null|string
     */
    public function getProcessNumber()
    {
        return $this->processNumber;
    }

    /**
     * @param string $processNumber
     */
    public function setProcessNumber(string $processNumber)
    {
        $this->processNumber = $processNumber;
    }

    /**
     * formRepository
     *
     * @var \KITT3N\Kitt3nForm\Domain\Repository\FormRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $formRepository = null;

    /**
     * sessionService
     *
     * @var \KITT3N\Kitt3nForm\Service\SessionService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $sessionService = null;

    /**
     * patternService
     *
     * @var \KITT3N\Kitt3nForm\Service\PatternService
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $patternService = null;

    /**
     * @param array $aSettings
     *
     * @return bool
     */
    public function initStorage($aSettings = [])
    {
        $aStorage = $this->getStorageArrayFromSettings($aSettings);
        if ($aStorage != null) {
            $bConfirmPath = $this->makeDirectoryRecursiveForAbsolutePath($aStorage['confirm']['path_absolute']);
            $this->setConfirmAbsolutePath($aStorage['confirm']['path_absolute']);
            $bPath = $this->makeDirectoryRecursiveForAbsolutePath($aStorage['path_absolute']);
            $this->setAbsolutePath($aStorage['path_absolute']);

            /*
             * Add .htaccess file to secure the folders
             */
            $fs = new Filesystem();
            $fs->dumpFile($aStorage['path_absolute'] . '.htaccess', "Deny from all ");
            $fs->dumpFile($aStorage['confirm']['path_absolute'] . '.htaccess', "Deny from all ");
        }
        if ($bConfirmPath and $bPath) {
            return true;
        }

        return false;
    }

    /**
     * @param array $aSettings
     * @param string $sSubmittedFormViewVarsName
     *
     * @return bool
     */
    public function initStorageForSubmittedForm($aSettings = [], $sSubmittedFormViewVarsName = '')
    {
        $aStorage = $this->getStorageArrayFromSettings($aSettings);
        if ($aStorage != null && $sSubmittedFormViewVarsName != '') {
            $this->initStorage($aSettings);
            /*
             * Extract 'iCurrentPageUid' and 'iSubmittedFormUid' submitted from name to get 'processNumber'
             * and create folder for submitted form.
             */
            $aConstArray = $this->patternService->getConstArray();
            $iSubmittedFormUid = $this->patternService->getValueFromFormNameByPlaceholder($sSubmittedFormViewVarsName, $aConstArray["FORM_PLACEHOLDER__FORM_UID"]);
            $sProcessNumberFromSession = $this->sessionService->getProcessNumberFromActiveSession($iSubmittedFormUid);
            if ($sProcessNumberFromSession != null) {
                $bSubmittedFormPath = $this->makeDirectoryRecursiveForAbsolutePath($aStorage['path_absolute'] . $sProcessNumberFromSession);
            } else {
                return false;
            }
        }
        if ($bSubmittedFormPath) {
            return true;
        }
        return false;
    }

    /**
     * @param array $aSettings
     *
     * @return array|mixed|null
     */
    public function getStorageArrayFromSettings($aSettings = [])
    {
        /*
         * Add '/' at the end of $_SERVER['DOCUMENT_ROOT']
         */
        $sServerDocumentRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/';

        $aStorage = [];
        if (is_array($aSettings) and count($aSettings) > 0) {
            if (array_key_exists('storage', $aSettings) and is_array($aSettings['storage'])) {
                if (array_key_exists('absolute_path_from_webroot', $aSettings['storage']) and $aSettings['storage']['absolute_path_from_webroot'] != '') {
                    if (array_key_exists('confirm',
                            $aSettings['storage']) and is_array($aSettings['storage']['confirm'])) {
                        if (array_key_exists('absolute_path_from_webroot',
                                $aSettings['storage']['confirm']) and $aSettings['storage']['confirm']['absolute_path_from_webroot'] != '') {

                            /*
                             * Use storage settings from Constants/Setup
                             */
                            $aStorage = $aSettings['storage'];

                            /*
                             * Make sure path begins and ends with a '/'
                             */
                            $aStorage['absolute_path_from_webroot'] =  "/" . ltrim($aStorage['absolute_path_from_webroot'], "/");
                            $aStorage['absolute_path_from_webroot'] = rtrim($aStorage['absolute_path_from_webroot'], "/") . "/";

                            /*
                             * Add absolute server path
                             * $_SERVER['DOCUMENT_ROOT'] should not be in 'path' but do nothing if it is already there
                             */
                            if (strpos($aStorage['absolute_path_from_webroot'], $sServerDocumentRoot) === 0) {
                                $sPathAbsolute = $aStorage['absolute_path_from_webroot'];
                            } else {
                                $sPathAbsolute = $sServerDocumentRoot . ltrim($aStorage['absolute_path_from_webroot'], "/");
                            }
                            $aStorage['path_absolute'] = $sPathAbsolute;

                            /*
                             * Form Uid
                             */
                            if (array_key_exists('oKitt3nForm',
                                    $aSettings['storage']) and $aSettings['storage']['oKitt3nForm'] != '') {
                                /* @var \KITT3N\Kitt3nForm\Domain\Model\Form $oForm */
                                $oForm = $this->formRepository->findByUid($aSettings['storage']['oKitt3nForm']);
                                if ($oForm != null) {
                                    $sConfirmPathOverride = trim($oForm->getStorageConfirmPath());
                                    if ($sConfirmPathOverride != "") {
                                        $aStorage['confirm']['absolute_path_from_webroot'] = $sConfirmPathOverride;
                                    }
                                }
                            }

                            /*
                             * Make sure path begins and ends with a '/'
                             */
                            $aStorage['confirm']['absolute_path_from_webroot'] = "/" . ltrim($aStorage['confirm']['absolute_path_from_webroot'], "/");
                            $aStorage['confirm']['absolute_path_from_webroot'] = rtrim($aStorage['confirm']['absolute_path_from_webroot'], "/") . "/";

                            /*
                             * Add confirmation absolute server path
                             * $_SERVER['DOCUMENT_ROOT'] should not be in 'path' but do nothing if it is already there
                             */
                            if (strpos($aStorage['confirm']['absolute_path_from_webroot'], $sServerDocumentRoot) === 0) {
                                $sPathConfirmAbsolute = $aStorage['confirm']['absolute_path_from_webroot'];
                            } else {
                                $sPathConfirmAbsolute = $sServerDocumentRoot . ltrim($aStorage['confirm']['absolute_path_from_webroot'], "/");
                            }
                            $aStorage['confirm']['path_absolute'] = $sPathConfirmAbsolute;

                            return $aStorage;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param string $sFilename
     *
     * @return string
     */
    private function getFilenameSanitized($sFilename = '')
    {
        $aConst = $this->patternService->getConstArray();
        $sFilenameSanitized = str_replace($aConst["FIELD__MANDATORY"], '', $sFilename);

        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;[]().
        // If you don't need to handle multi-byte characters
        // you can use preg_replace rather than mb_ereg_replace
        // Thanks @Łukasz Rysiak!
        $sFilenameSanitized = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $sFilenameSanitized);
        // Remove any runs of periods (thanks falstro!)
        $sFilenameSanitized = mb_ereg_replace("([\.]{2,})", '', $sFilenameSanitized);

        return strtolower($sFilenameSanitized);
    }

    /**
     * @param string $sFilename
     *
     * @return string
     */
    private function getFileExtension($sFilename = '')
    {
        $aFilename = [];
        // split it into part by dot
        $aFilename = explode('.', $sFilename);

        if (count($aFilename) > 1) {
            // get the last one
            return $aFilename[count($aFilename) - 1];
        }

        return '';

    }

    /**
     * @param string $sAbsolutePath
     *
     * @return bool
     */
    private function makeDirectoryRecursiveForAbsolutePath($sAbsolutePath = '')
    {
        /*
         * Add '/' at the end of $_SERVER['DOCUMENT_ROOT']
         */
        $sServerWebDocumentRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/');

        if (strpos($sAbsolutePath, $sServerWebDocumentRoot) !== false) {
            /*
             * Make sure path ends with a '/'
             */
            $sAbsolutePath = rtrim($sAbsolutePath, "/") . "/";

            /*
             * New Filesystem
             */
            $fs = new Filesystem();

            /*
             * Create directory if it does not exist
             */
            if ($fs->exists($sAbsolutePath)) {
                return true;
            } else {
                try {
                    $fs->mkdir($sAbsolutePath);
                } catch (IOExceptionInterface $e) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $sFileToRemove
     * @param string $sAbsolutePath
     *
     * @return bool
     */
    private function removeFileFromPath($sFileToRemove = '', $sAbsolutePath = '')
    {
        if ($sFileToRemove != '' and $sAbsolutePath != '') {
            $finder = new Finder();
            $finder->followLinks(true);
            $files = $finder->files()->name($sFileToRemove)->in($sAbsolutePath);
            if ($files->hasResults()) {
                foreach ($files as $file) {
                    $sFileRelativePathname = $file->getRelativePathname();
                    /*
                     * Remove file
                     */
                    $fs = new Filesystem();
                    /*
                     * Check if file exists just to be sure
                     */
                    if ($fs->exists($sAbsolutePath . $sFileRelativePathname)) {
                        try {
                            $fs->remove($sAbsolutePath . $sFileRelativePathname);
                        } catch (IOExceptionInterface $e) {
                            return false;
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            return false;
        }
        return false;
    }

    /**
     * @param string $sToken
     * @param array $aSettings
     *
     * @return array
     */
    public function mirrorSubmittedDataAfterConfirmation($sToken = '', $aSettings = [])
    {
        if ( ! empty($sToken) and count($aSettings) > 0) {
            $aStorage = $this->getStorageArrayFromSettings($aSettings);
            $sOrigDir = $aStorage['path_absolute'] . $sToken;
            $sTargetDir = $aStorage['confirm']['path_absolute'];
            if ($aStorage != null) {
                $fs = new Filesystem();
                if ($fs->exists($aStorage['path_absolute']) and $fs->exists($aStorage['confirm']['path_absolute']) and $fs->exists($sTargetDir . $sToken) and ! $fs->exists($sOrigDir)) {
                    /*
                     * Already done
                     */
                    return [2, 'Internal reference: 2-' . __LINE__];
                }
                if ($fs->exists($sOrigDir) and $fs->exists($sTargetDir)) {
                    try {
                        $fs->mkdir($sTargetDir . $sToken);
                        $fs->mirror($sOrigDir, $sTargetDir . $sToken);
                        $fs->touch($sTargetDir . $sToken . '/' . time() .  '__' . $_SERVER['REMOTE_ADDR'] . '.log');
                        $fs->remove($sOrigDir);
                    } catch (IOExceptionInterface $e) {
                        /*
                         * IOException
                         */
                        return [3, 'Error reference: 3-' .  __LINE__];
                    }
                    /*
                     * Done
                     */
                    return [1, 'Internal reference: 1-' .  __LINE__];
                } else {
                    /*
                     * Configuration error
                     */
                    return [4, 'Error reference: 4-' .  __LINE__];
                }
            }
        }
        /*
         * Configuration
         */
        return [5,'Error reference: 5-' .  __LINE__];
    }


    /**
     * @param \Symfony\Component\Form\FormView $oSubmittedFormView
     * @param array $aSettings
     *
     * @return bool
     */
    public function moveUploadedFiles($oSubmittedFormView, $aSettings = [])
    {

        $bM = false;
        $aStorage = $this->getStorageArrayFromSettings($aSettings);
        switch ($aSettings) {
            case null:
                return $bM;
                break;
            default:
                foreach ($oSubmittedFormView->children as $oFormFormView) {
                    if (array_key_exists('block_prefixes', $oFormFormView->vars)) {
                        if ($oFormFormView->vars['block_prefixes'][1] == 'file') {
                            if ($oFormFormView->vars['data'] != null) {
                                /*
                                 * Sanitize filename
                                 */
                                $sFileBasnameSanitized = $this->getFilenameSanitized($oFormFormView->vars['name'] . "__" . $oFormFormView->vars['label']);
                                $sFilenameSanitized = $sFileBasnameSanitized .
                                                      ($this->getFileExtension($oFormFormView->vars['data']['name']) != '' ?
                                                          '.' . $this->getFileExtension($oFormFormView->vars['data']['name']) : '');

                                /*
                                 * Extract iPageUid and iFormUid from field name.
                                 */
                                $aConstArray = $this->patternService->getConstArray();
                                $iFormUid = $this->patternService->getValueFromFieldNameByPlaceholder($oFormFormView->vars['name'], $aConstArray["FIELD_PLACEHOLDER__FORM_UID"]);
                                /*
                                 * Get processNumber from session
                                 */
                                if ($iFormUid != null) {
                                    $sProcessNumber = $this->sessionService->getProcessNumberFromActiveSession($iFormUid);
                                    $sDestinationPath =  $aStorage["path_absolute"] . $sProcessNumber . "/";
                                    /*
                                     * New Filesystem
                                     */
                                    $fs = new Filesystem();
                                    $fs->copy($oFormFormView->vars['data']['tmp_name'],$sDestinationPath . $sFilenameSanitized, true);
//                                    if(! move_uploaded_file($oFormFormView->vars['data']['tmp_name'],$sDestinationPath . $sFilenameSanitized)) {
//                                        throw new \Exception(__CLASS__ . " :: " . __FUNCTION__ . " :: " . __LINE__);
//                                    }
                                    $this->sessionService->saveParameterIntoActiveSession($oFormFormView->vars['name'], $sFilenameSanitized);
                                } else {
                                    throw new \Exception(__CLASS__ . " :: " . __FUNCTION__ . " :: " . __LINE__);
                                }
                            } else {
                                $this->sessionService->saveParameterIntoActiveSession($oFormFormView->vars['name'], '');

                                /*
                                 * Sanitize filename
                                 */
                                $sFileBasnameSanitized = $this->getFilenameSanitized($oFormFormView->vars['name'] . "__" . $oFormFormView->vars['label']);

                                if ($this->getProcessNumber() != null) {
                                    $sFileToRemove = $sFileBasnameSanitized . '.*';
                                    $sFromPath = $aStorage["path_absolute"] . $this->getProcessNumber() . "/";
                                    $this->removeFileFromPath($sFileToRemove, $sFromPath);
                                }

                            }
                        }
                    }
                }
        }

        return $bM;
    }

    /**
     * saveCurrentSessionViaFormViewIntoFile
     *
     * Iterate over a aFormView array to save all fields that are already saved in the current session
     * as JSON (and XML). This is important because each step is technically a standalone form which should
     * be saved on submit.
     *
     * @param \Symfony\Component\Form\FormView $aFormView
     * @param integer $iSubmittedStep
     * @param string $sProcessnumber
     */
    public function saveCurrentSessionViaFormViewIntoFile ($aFormView, $iSubmittedStep = 1, $sProcessnumber = '')
    {
        $activeSession = $this->sessionService->getActiveSession();
        $aSessionAttributes = $activeSession->getBag('attributes')->all();
        $aSessionAttributesClean = [];

        $aConst = $this->patternService->getConstArray();

        $aSave = [];
        $aSaveXml = []; $aSaveXml["step"] = [];
        $aSaveXmlField = [];

        $aSaveYaml = []; $aSaveYaml["steps"] = [];
        $aSaveYamlField = [];

        /*
         * Iterate over steps
         * => foreach $aFormView
         */
        foreach ($aFormView as $iStep => $step) {
            $aSave[$iStep] = [];

            $aSaveXmlField["field"] = [];
            $aSaveYamlField["fields"] = [];
            /*
             * iterate over children
             */
            foreach ($aFormView[$iStep]->children as $sFieldName => $oField) {
                /*
                 * TODO save label and value if present in session array.
                 * Could be present or not, because each step of a multistep form could have been submitted right now
                 *
                 * [
                 *  "label" => use label from formView
                 *  "value" => use value from activeSession
                 * ]
                 */
                if (substr($sFieldName, 0, 2) != $aConst['CONTROL_FIELD_PREFIX']) {
                    if (strpos($sFieldName, $aConst['HELPER_FIELD_SUFFIX']) !== false) {
                        // ignore helper fields
                    } else {
                        if (array_key_exists($sFieldName, $aSessionAttributes)) {
                            $aSave[$iStep][] = [
                                "type" => $oField->vars['block_prefixes'][1],
                                "label" => $oField->vars["label"],
                                "value" => $aSessionAttributes[$sFieldName]
                            ];
                            $aSaveXmlField["field"][] = [
                                "@name" => $sFieldName,
                                "@type" => $oField->vars['block_prefixes'][1],
                                "label" => $oField->vars["label"],
                                "value" => $aSessionAttributes[$sFieldName]
                            ];
                            $aSaveYamlField["fields"][] = [
                                "name" => $sFieldName,
                                "type" => $oField->vars['block_prefixes'][1],
                                "label" => $oField->vars["label"],
                                "value" => $aSessionAttributes[$sFieldName]
                            ];
                            $aSessionAttributesClean[$sFieldName] = $aSessionAttributes[$sFieldName];
                        } else {
                            /*
                             * Fields not in session
                             */
                            $aSave[$iStep][] = [
                                "type" => $oField->vars['block_prefixes'][1],
                                "label" => $oField->vars["label"],
                                "value" => ""
                            ];
                            $aSaveXmlField["field"][] = [
                                "@name" => $sFieldName,
                                "@type" => $oField->vars['block_prefixes'][1],
                                "label" => $oField->vars["label"],
                                "value" => ""
                            ];
                            $aSaveYamlField["fields"][] = [
                                "name" => $sFieldName,
                                "type" => $oField->vars['block_prefixes'][1],
                                "label" => $oField->vars["label"],
                                "value" => ""
                            ];
                        }
                    }
                }
            }
            $aSaveXml["step"][$iStep] = $aSaveXmlField;
            $aSaveYaml["steps"][$iStep] = $aSaveYamlField;
        }

//        /*
//         * Extract iPageUid and iFormUid from form name.
//         */
//        $aConstArray = $this->patternService->getConstArray();
//        $iPageUid = $this->patternService->getValueFromFieldNameByPlaceholder($aFormView[$iSubmittedStep]->vars['name'], $aConstArray["FORM_PLACEHOLDER__PAGE_UID"]);
//        $iFormUid = $this->patternService->getValueFromFieldNameByPlaceholder($aFormView[$iSubmittedStep]->vars['name'], $aConstArray["FORM_PLACEHOLDER__FORM_UID"]);
//        $sFormSeparator = $aConstArray["FORM_SEPARATOR"];
//
//        /*
//         *
//         */
//        $aFieldPattern = $this->patternService->getFieldPatternArray();
//
//        if ($iPageUid != null and $iFormUid != null) {
//            $sProcessNumber = $this->sessionService->getProcessNumberFromActiveSession($iPageUid, $iFormUid);
//
//            $aField = [];
//            $aFieldset = [];
//            $aStep = [];
//            foreach ($aAttribute as $key => $value) {
//                /*
//                 * Is a field and not e.g. a processNumber
//                 */
//                if (count($key) == count($aFieldPattern)) {
//                    $aKey = explode($aConstArray["FIELD_SEPARATOR"], $key);
//                    /*
//                     * Is field in submitted form?
//                     */
//                    if ($aKey[$aFieldPattern[$aConstArray["FIELD_PLACEHOLDER__PAGE_UID"]]] == $iPageUid and $aKey[$aFieldPattern[$aConstArray["FIELD_PLACEHOLDER__FORM_UID"]]] == $iFormUid) {
//                        //$aSave[$aKey[$aFieldPattern[$aConstArray["FIELD_PLACEHOLDER__PAGE_UID"]]]] = [];
//                    }
//                }
//            }
//
//        }

        $encoders = array(new XmlEncoder(), new JsonEncoder(), new YamlEncoder());
//        $normalizers = array(new ObjectNormalizer());
//
        $serializer = new Serializer($aSave, $encoders);
        $xmlContent = $serializer->serialize($aSaveXml, 'xml');
        $jsonContent = $serializer->serialize($aSaveYaml, 'json');
        $yamlContent = $serializer->serialize($aSaveYaml, 'yaml');

        $serializedPhpArrayContent = serialize($aSaveYaml);

        $aSessionAttributesCleanSerialized = serialize($aSessionAttributesClean);

        /*
         * New Filesystem
         */
        $fs = new Filesystem();
        $fsp = $this->getAbsolutePath() . $sProcessnumber . '/';

        /*
         * Save data in different machine and human readable formats
         */
        $fs->dumpFile($fsp . 'data.xml', $xmlContent);
        $fs->dumpFile($fsp . 'data.json', $jsonContent);
        $fs->dumpFile($fsp . 'data.yml', $yamlContent);

        /*
         * Because it's easier to work with php arrays in Services or Controller
         * save data as serialized php arrays.
         */
        $fs->dumpFile($fsp . 'data.serialized.txt', $serializedPhpArrayContent);
        $fs->dumpFile($fsp . 'session.serialized.txt', $aSessionAttributesCleanSerialized);

        /*
         * Add more security
         */
        $fs->dumpFile($fsp . '.htaccess', "Deny from all ");

    }
}