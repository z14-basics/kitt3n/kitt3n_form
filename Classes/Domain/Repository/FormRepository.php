<?php
namespace KITT3N\Kitt3nForm\Domain\Repository;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Forms
 */
class FormRepository extends \KITT3N\Kitt3nForm\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\Form';
        $sUserFuncPlugin = 'tx_kitt3nform';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
