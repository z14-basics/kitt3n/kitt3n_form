<?php
namespace KITT3N\Kitt3nForm\Domain\Repository;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for MailToReceivers
 */
class MailToReceiverRepository extends \KITT3N\Kitt3nForm\Domain\Repository\AbstractRepository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject()
    {
        $sUserFuncModel = 'KITT3N\\Kitt3nForm\\Domain\\Model\\MailToReceiver';
        $sUserFuncPlugin = 'tx_kitt3nform';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
