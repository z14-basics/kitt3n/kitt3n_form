<?php
namespace KITT3N\Kitt3nForm\Domain\Repository;
use KITT3N\Kitt3nUserfuncs\UserFunc\StorageUserFunc;
/***
 *
 * This file should be part of the "kitt3n_repository" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Abstract Repository
 */
class AbstractRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @param $sUserFuncModel
     * @param $sPlugin
     */
    public function overrideQuerySettings ($sUserFuncModel, $sPlugin) {
        /*
         * Get storage pid list as comma separated string
         * for given Model in given Plugin
         */
        $sStoragePageIds = StorageUserFunc::getStoragePidListForModelInPlugin($sUserFuncModel, $sPlugin);

        /* @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');

        /*
         * If storage pid list is empty
         * do not respect StoragePage
         * and instead get Entities from
         * any page
         */
        if ($sStoragePageIds == '') {
            $querySettings->setRespectStoragePage(FALSE);
        } else {
            /*
             * Fallback in case we need to respect the StoragePage
             */
            $querySettings->setRespectStoragePage(TRUE);
            $querySettings->setStoragePageIds(explode(",", $sStoragePageIds));
        }

        /*
         * Set default QuerySettings
         */
        $this->setDefaultQuerySettings($querySettings);
    }
}
