<?php
namespace KITT3N\Kitt3nForm\Domain\Model;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * MailToReceiver
 */
class MailToReceiver extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * fromName
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromName = '';

    /**
     * fromMail
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromMail = '';

    /**
     * fromSubject
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromSubject = '';

    /**
     * toName
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $toName = '';

    /**
     * toMail
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $toMail = '';

    /**
     * toCc
     *
     * @var string
     */
    protected $toCc = '';

    /**
     * toBcc
     *
     * @var string
     */
    protected $toBcc = '';

    /**
     * appendData
     *
     * @var bool
     */
    protected $appendData = false;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the fromName
     *
     * @return string $fromName
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Sets the fromName
     *
     * @param string $fromName
     * @return void
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }

    /**
     * Returns the fromMail
     *
     * @return string $fromMail
     */
    public function getFromMail()
    {
        return $this->fromMail;
    }

    /**
     * Sets the fromMail
     *
     * @param string $fromMail
     * @return void
     */
    public function setFromMail($fromMail)
    {
        $this->fromMail = $fromMail;
    }

    /**
     * Returns the fromSubject
     *
     * @return string $fromSubject
     */
    public function getFromSubject()
    {
        return $this->fromSubject;
    }

    /**
     * Sets the fromSubject
     *
     * @param string $fromSubject
     * @return void
     */
    public function setFromSubject($fromSubject)
    {
        $this->fromSubject = $fromSubject;
    }

    /**
     * Returns the toName
     *
     * @return string $toName
     */
    public function getToName()
    {
        return $this->toName;
    }

    /**
     * Sets the toName
     *
     * @param string $toName
     * @return void
     */
    public function setToName($toName)
    {
        $this->toName = $toName;
    }

    /**
     * Returns the toMail
     *
     * @return string $toMail
     */
    public function getToMail()
    {
        return $this->toMail;
    }

    /**
     * Sets the toMail
     *
     * @param string $toMail
     * @return void
     */
    public function setToMail($toMail)
    {
        $this->toMail = $toMail;
    }

    /**
     * Returns the toCc
     *
     * @return string $toCc
     */
    public function getToCc()
    {
        return $this->toCc;
    }

    /**
     * Sets the toCc
     *
     * @param string $toCc
     * @return void
     */
    public function setToCc($toCc)
    {
        $this->toCc = $toCc;
    }

    /**
     * Returns the toBcc
     *
     * @return string $toBcc
     */
    public function getToBcc()
    {
        return $this->toBcc;
    }

    /**
     * Sets the toBcc
     *
     * @param string $toBcc
     * @return void
     */
    public function setToBcc($toBcc)
    {
        $this->toBcc = $toBcc;
    }

    /**
     * Returns the appendData
     *
     * @return bool $appendData
     */
    public function getAppendData()
    {
        return $this->appendData;
    }

    /**
     * Sets the appendData
     *
     * @param bool $appendData
     * @return void
     */
    public function setAppendData($appendData)
    {
        $this->appendData = $appendData;
    }

    /**
     * Returns the boolean state of appendData
     *
     * @return bool
     */
    public function isAppendData()
    {
        return $this->appendData;
    }
}
