<?php
namespace KITT3N\Kitt3nForm\Domain\Model;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Constraint
 */
class Constraint extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * mandatory
     *
     * @var bool
     */
    protected $mandatory = false;

    /**
     * minLength
     *
     * @var int
     */
    protected $minLength = 0;

    /**
     * maxLength
     *
     * @var int
     */
    protected $maxLength = 0;

    /**
     * mimeType
     *
     * @var string
     */
    protected $mimeType = '';

    /**
     * maxFileSize
     *
     * @var float
     */
    protected $maxFileSize = 0.0;

    /**
     * maxFileSizeUnit
     *
     * @var string
     */
    protected $maxFileSizeUnit = '';

    /**
     * form
     *
     * @var \KITT3N\Kitt3nForm\Domain\Model\Form
     */
    protected $form = null;

    /**
     * field
     *
     * @var \KITT3N\Kitt3nForm\Domain\Model\Field
     */
    protected $field = null;

    /**
     * Returns the mandatory
     *
     * @return bool $mandatory
     */
    public function getMandatory()
    {
        return $this->mandatory;
    }

    /**
     * Sets the mandatory
     *
     * @param bool $mandatory
     * @return void
     */
    public function setMandatory($mandatory)
    {
        $this->mandatory = $mandatory;
    }

    /**
     * Returns the boolean state of mandatory
     *
     * @return bool
     */
    public function isMandatory()
    {
        return $this->mandatory;
    }

    /**
     * Returns the minLength
     *
     * @return int $minLength
     */
    public function getMinLength()
    {
        return $this->minLength;
    }

    /**
     * Sets the minLength
     *
     * @param int $minLength
     * @return void
     */
    public function setMinLength($minLength)
    {
        $this->minLength = $minLength;
    }

    /**
     * Returns the form
     *
     * @return \KITT3N\Kitt3nForm\Domain\Model\Form $form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Sets the form
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\Form $form
     * @return void
     */
    public function setForm(\KITT3N\Kitt3nForm\Domain\Model\Form $form)
    {
        $this->form = $form;
    }

    /**
     * Returns the field
     *
     * @return \KITT3N\Kitt3nForm\Domain\Model\Field $field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Sets the field
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\Field $field
     * @return void
     */
    public function setField(\KITT3N\Kitt3nForm\Domain\Model\Field $field)
    {
        $this->field = $field;
    }

    /**
     * Returns the maxLength
     *
     * @return int maxLength
     */
    public function getMaxLength()
    {
        return $this->maxLength;
    }

    /**
     * Sets the maxLength
     *
     * @param string $maxLength
     * @return void
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;
    }

    /**
     * Returns the mimeType
     *
     * @return string $mimeType
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Sets the mimeType
     *
     * @param string $mimeType
     * @return void
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

    /**
     * Returns the maxFileSize
     *
     * @return float $maxFileSize
     */
    public function getMaxFileSize()
    {
        return $this->maxFileSize;
    }

    /**
     * Sets the maxFileSize
     *
     * @param float $maxFileSize
     * @return void
     */
    public function setMaxFileSize($maxFileSize)
    {
        $this->maxFileSize = $maxFileSize;
    }

    /**
     * Returns the maxFileSizeUnit
     *
     * @return string $maxFileSizeUnit
     */
    public function getMaxFileSizeUnit()
    {
        return $this->maxFileSizeUnit;
    }

    /**
     * Sets the maxFileSizeUnit
     *
     * @param string $maxFileSizeUnit
     * @return void
     */
    public function setMaxFileSizeUnit($maxFileSizeUnit)
    {
        $this->maxFileSizeUnit = $maxFileSizeUnit;
    }
}
