<?php
namespace KITT3N\Kitt3nForm\Domain\Model;


/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
/**
 * Fieldset
 */
class Fieldset extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * class
     * 
     * @var string
     */
    protected $class = '';

    /**
     * field
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Field>
     */
    protected $field = null;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->field = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Adds a Field
     * 
     * @param \KITT3N\Kitt3nForm\Domain\Model\Field $field
     * @return void
     */
    public function addField(\KITT3N\Kitt3nForm\Domain\Model\Field $field)
    {
        $this->field->attach($field);
    }

    /**
     * Removes a Field
     * 
     * @param \KITT3N\Kitt3nForm\Domain\Model\Field $fieldToRemove The Field to be removed
     * @return void
     */
    public function removeField(\KITT3N\Kitt3nForm\Domain\Model\Field $fieldToRemove)
    {
        $this->field->detach($fieldToRemove);
    }

    /**
     * Returns the field
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Field> $field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Sets the field
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Field> $field
     * @return void
     */
    public function setField(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $field)
    {
        $this->field = $field;
    }

    /**
     * Returns the class
     * 
     * @return string $class
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Sets the class
     * 
     * @param string $class
     * @return void
     */
    public function setClass($class)
    {
        $this->class = $class;
    }
}
