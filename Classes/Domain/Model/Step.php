<?php
namespace KITT3N\Kitt3nForm\Domain\Model;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Step
 */
class Step extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * fieldset
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Fieldset>
     */
    protected $fieldset = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->fieldset = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Adds a Fieldset
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\Fieldset $fieldset
     * @return void
     */
    public function addFieldset(\KITT3N\Kitt3nForm\Domain\Model\Fieldset $fieldset)
    {
        $this->fieldset->attach($fieldset);
    }

    /**
     * Removes a Fieldset
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\Fieldset $fieldsetToRemove The Fieldset to be removed
     * @return void
     */
    public function removeFieldset(\KITT3N\Kitt3nForm\Domain\Model\Fieldset $fieldsetToRemove)
    {
        $this->fieldset->detach($fieldsetToRemove);
    }

    /**
     * Returns the fieldset
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Fieldset> $fieldset
     */
    public function getFieldset()
    {
        return $this->fieldset;
    }

    /**
     * Sets the fieldset
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Fieldset> $fieldset
     * @return void
     */
    public function setFieldset(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $fieldset)
    {
        $this->fieldset = $fieldset;
    }
}
