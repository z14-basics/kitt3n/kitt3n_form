<?php
namespace KITT3N\Kitt3nForm\Domain\Model;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Form
 */
class Form extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * textSubmit
     *
     * @var string
     */
    protected $textSubmit = '';

    /**
     * textConfirm
     *
     * @var string
     */
    protected $textConfirm = '';

    /**
     * fieldSenderFirstName
     *
     * @var string
     */
    protected $fieldSenderFirstName = '';

    /**
     * fieldSenderName
     *
     * @var string
     */
    protected $fieldSenderName = '';

    /**
     * fieldSenderMail
     *
     * @var string
     */
    protected $fieldSenderMail = '';

    /**
     * storagePath
     *
     * @var string
     */
    protected $storagePath = '';

    /**
     * storageConfirmPath
     *
     * @var string
     */
    protected $storageConfirmPath = '';

    /**
     * step
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Step>
     */
    protected $step = null;

    /**
     * mailToSenderConfirm
     *
     * @var \KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm
     */
    protected $mailToSenderConfirm = null;

    /**
     * mailToSender
     *
     * @var \KITT3N\Kitt3nForm\Domain\Model\MailToSender
     */
    protected $mailToSender = null;

    /**
     * mailToReceiver
     *
     * @var \KITT3N\Kitt3nForm\Domain\Model\MailToReceiver
     */
    protected $mailToReceiver = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->step = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Step
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\Step $step
     * @return void
     */
    public function addStep(\KITT3N\Kitt3nForm\Domain\Model\Step $step)
    {
        $this->step->attach($step);
    }

    /**
     * Removes a Step
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\Step $stepToRemove The Step to be removed
     * @return void
     */
    public function removeStep(\KITT3N\Kitt3nForm\Domain\Model\Step $stepToRemove)
    {
        $this->step->detach($stepToRemove);
    }

    /**
     * Returns the step
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Step> $step
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Sets the step
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KITT3N\Kitt3nForm\Domain\Model\Step> $step
     * @return void
     */
    public function setStep(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $step)
    {
        $this->step = $step;
    }

    /**
     * Returns the mailToSender
     *
     * @return \KITT3N\Kitt3nForm\Domain\Model\MailToSender $mailToSender
     */
    public function getMailToSender()
    {
        return $this->mailToSender;
    }

    /**
     * Sets the mailToSender
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\MailToSender $mailToSender
     * @return void
     */
    public function setMailToSender(\KITT3N\Kitt3nForm\Domain\Model\MailToSender $mailToSender)
    {
        $this->mailToSender = $mailToSender;
    }

    /**
     * Returns the mailToSenderConfirm
     *
     * @return \KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm $mailToSenderConfirm
     */
    public function getMailToSenderConfirm()
    {
        return $this->mailToSenderConfirm;
    }

    /**
     * Sets the mailToSenderConfirm
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm $mailToSenderConfirm
     * @return void
     */
    public function setMailToSenderConfirm(\KITT3N\Kitt3nForm\Domain\Model\MailToSenderConfirm $mailToSenderConfirm)
    {
        $this->mailToSenderConfirm = $mailToSenderConfirm;
    }

    /**
     * Returns the mailToReceiver
     *
     * @return \KITT3N\Kitt3nForm\Domain\Model\MailToReceiver $mailToReceiver
     */
    public function getMailToReceiver()
    {
        return $this->mailToReceiver;
    }

    /**
     * Sets the mailToReceiver
     *
     * @param \KITT3N\Kitt3nForm\Domain\Model\MailToReceiver $mailToReceiver
     * @return void
     */
    public function setMailToReceiver(\KITT3N\Kitt3nForm\Domain\Model\MailToReceiver $mailToReceiver)
    {
        $this->mailToReceiver = $mailToReceiver;
    }

    /**
     * Returns the textSubmit
     *
     * @return string $textSubmit
     */
    public function getTextSubmit()
    {
        return $this->textSubmit;
    }

    /**
     * Sets the textSubmit
     *
     * @param string $textSubmit
     * @return void
     */
    public function setTextSubmit($textSubmit)
    {
        $this->textSubmit = $textSubmit;
    }

    /**
     * Returns the textConfirm
     *
     * @return string $textConfirm
     */
    public function getTextConfirm()
    {
        return $this->textConfirm;
    }

    /**
     * Sets the textConfirm
     *
     * @param string $textConfirm
     * @return void
     */
    public function setTextConfirm($textConfirm)
    {
        $this->textConfirm = $textConfirm;
    }

    /**
     * Returns the fieldSenderFirstName
     *
     * @return string $fieldSenderFirstName
     */
    public function getFieldSenderFirstName()
    {
        return $this->fieldSenderFirstName;
    }

    /**
     * Sets the fieldSenderFirstName
     *
     * @param string $fieldSenderFirstName
     * @return void
     */
    public function setFieldSenderFirstName($fieldSenderFirstName)
    {
        $this->fieldSenderFirstName = $fieldSenderFirstName;
    }

    /**
     * Returns the fieldSenderName
     *
     * @return string $fieldSenderName
     */
    public function getFieldSenderName()
    {
        return $this->fieldSenderName;
    }

    /**
     * Sets the fieldSenderName
     *
     * @param string $fieldSenderName
     * @return void
     */
    public function setFieldSenderName($fieldSenderName)
    {
        $this->fieldSenderName = $fieldSenderName;
    }

    /**
     * Returns the fieldSenderMail
     *
     * @return string $fieldSenderMail
     */
    public function getFieldSenderMail()
    {
        return $this->fieldSenderMail;
    }

    /**
     * Sets the fieldSenderMail
     *
     * @param string $fieldSenderMail
     * @return void
     */
    public function setFieldSenderMail($fieldSenderMail)
    {
        $this->fieldSenderMail = $fieldSenderMail;
    }

    /**
     * Returns the storagePath
     *
     * @return string $storagePath
     */
    public function getStoragePath()
    {
        return $this->storagePath;
    }

    /**
     * Sets the storagePath
     *
     * @param string $storagePath
     * @return void
     */
    public function setStoragePath($storagePath)
    {
        $this->storagePath = $storagePath;
    }

    /**
     * Returns the storageConfirmPath
     *
     * @return string $storageConfirmPath
     */
    public function getStorageConfirmPath()
    {
        return $this->storageConfirmPath;
    }

    /**
     * Sets the storageConfirmPath
     *
     * @param string $storageConfirmPath
     * @return void
     */
    public function setStorageConfirmPath($storageConfirmPath)
    {
        $this->storageConfirmPath = $storageConfirmPath;
    }
}
