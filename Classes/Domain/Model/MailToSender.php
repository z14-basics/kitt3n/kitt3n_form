<?php
namespace KITT3N\Kitt3nForm\Domain\Model;

/***
 *
 * This file is part of the "kitt3n_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * MailToSender
 */
class MailToSender extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title = '';

    /**
     * fromName
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromName = '';

    /**
     * fromMail
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromMail = '';

    /**
     * fromSubject
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromSubject = '';

    /**
     * fromSalutation
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromSalutation = '';

    /**
     * fromBody
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromBody = '';

    /**
     * fromComplimentaryClose
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromComplimentaryClose = '';

    /**
     * fromFooter
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $fromFooter = '';

    /**
     * replyToName
     *
     * @var string
     */
    protected $replyToName = '';

    /**
     * replyToMail
     *
     * @var string
     */
    protected $replyToMail = '';

    /**
     * appendData
     *
     * @var bool
     */
    protected $appendData = false;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the fromName
     *
     * @return string $fromName
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Sets the fromName
     *
     * @param string $fromName
     * @return void
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }

    /**
     * Returns the fromMail
     *
     * @return string $fromMail
     */
    public function getFromMail()
    {
        return $this->fromMail;
    }

    /**
     * Sets the fromMail
     *
     * @param string $fromMail
     * @return void
     */
    public function setFromMail($fromMail)
    {
        $this->fromMail = $fromMail;
    }

    /**
     * Returns the fromSubject
     *
     * @return string $fromSubject
     */
    public function getFromSubject()
    {
        return $this->fromSubject;
    }

    /**
     * Sets the fromSubject
     *
     * @param string $fromSubject
     * @return void
     */
    public function setFromSubject($fromSubject)
    {
        $this->fromSubject = $fromSubject;
    }

    /**
     * Returns the fromSalutation
     *
     * @return string $fromSalutation
     */
    public function getFromSalutation()
    {
        return $this->fromSalutation;
    }

    /**
     * Sets the fromSalutation
     *
     * @param string $fromSalutation
     * @return void
     */
    public function setFromSalutation($fromSalutation)
    {
        $this->fromSalutation = $fromSalutation;
    }

    /**
     * Returns the fromBody
     *
     * @return string $fromBody
     */
    public function getFromBody()
    {
        return $this->fromBody;
    }

    /**
     * Sets the fromBody
     *
     * @param string $fromBody
     * @return void
     */
    public function setFromBody($fromBody)
    {
        $this->fromBody = $fromBody;
    }

    /**
     * Returns the fromFooter
     *
     * @return string $fromFooter
     */
    public function getFromFooter()
    {
        return $this->fromFooter;
    }

    /**
     * Sets the fromFooter
     *
     * @param string $fromFooter
     * @return void
     */
    public function setFromFooter($fromFooter)
    {
        $this->fromFooter = $fromFooter;
    }

    /**
     * Returns the replyToName
     *
     * @return string $replyToName
     */
    public function getReplyToName()
    {
        return $this->replyToName;
    }

    /**
     * Sets the replyToName
     *
     * @param string $replyToName
     * @return void
     */
    public function setReplyToName($replyToName)
    {
        $this->replyToName = $replyToName;
    }

    /**
     * Returns the replyToMail
     *
     * @return string $replyToMail
     */
    public function getReplyToMail()
    {
        return $this->replyToMail;
    }

    /**
     * Sets the replyToMail
     *
     * @param string $replyToMail
     * @return void
     */
    public function setReplyToMail($replyToMail)
    {
        $this->replyToMail = $replyToMail;
    }

    /**
     * Returns the fromComplimentaryClose
     *
     * @return string fromComplimentaryClose
     */
    public function getFromComplimentaryClose()
    {
        return $this->fromComplimentaryClose;
    }

    /**
     * Sets the fromComplimentaryClose
     *
     * @param string $fromComplimentaryClose
     * @return void
     */
    public function setFromComplimentaryClose($fromComplimentaryClose)
    {
        $this->fromComplimentaryClose = $fromComplimentaryClose;
    }

    /**
     * Returns the appendData
     *
     * @return bool $appendData
     */
    public function getAppendData()
    {
        return $this->appendData;
    }

    /**
     * Sets the appendData
     *
     * @param bool $appendData
     * @return void
     */
    public function setAppendData($appendData)
    {
        $this->appendData = $appendData;
    }

    /**
     * Returns the boolean state of appendData
     *
     * @return bool
     */
    public function isAppendData()
    {
        return $this->appendData;
    }
}
